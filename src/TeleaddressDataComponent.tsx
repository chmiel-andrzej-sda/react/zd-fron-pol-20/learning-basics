import React from "react";
import { TeleadressData } from "./entity/TeleaddressData";

export interface TeleaddressDataComponentProps {
    readonly teleaddressData: TeleadressData;
}

export function TeleaddressDataComponent(props: TeleaddressDataComponentProps): JSX.Element {
    return <div>
        {props.teleaddressData.street}, {props.teleaddressData.city}, {props.teleaddressData.country}
    </div>;
}

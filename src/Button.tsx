import React from 'react';

interface ButtonProps {
	readonly onClick: (count: number) => void;
	readonly defaultLabel: string;
	readonly start?: number;
	readonly reverse?: boolean;
	readonly count: number;
}

export function Button(props: ButtonProps): JSX.Element {
	function handleClick() {
		if (props.reverse) {
			props.onClick(props.count - 1);
		} else {
			props.onClick(props.count + 1);
		}
	}

	return <div>
		<button onClick={handleClick}>{props.count === props.start ? props.defaultLabel : props.count}</button>
	</div>;
}

import React from "react";

interface GreetingProps {
	readonly name: string;
}

export function Greeting(props: GreetingProps): JSX.Element {
	return <div>Greeting, {props.name}</div>;
}

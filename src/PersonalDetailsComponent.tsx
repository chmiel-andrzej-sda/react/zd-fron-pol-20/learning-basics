import React from "react";
import { PersonalDetails } from "./entity/PersonalDetails";

export interface PersonalDetailsComponentProps {
	readonly personalDetails: PersonalDetails;
}

export function PersonalDetailsComponent(props: PersonalDetailsComponentProps): JSX.Element {
	return <div>
		{props.personalDetails.firstName} {props.personalDetails.lastName} ({props.personalDetails.pesel})
	</div>;
}

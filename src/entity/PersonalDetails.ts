export interface PersonalDetails {
	readonly firstName: string;
	readonly lastName: string;
	readonly pesel: number;
}
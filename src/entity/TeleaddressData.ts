export interface TeleadressData {
    readonly street?: string;
    readonly city: string;
    readonly country: string;
}

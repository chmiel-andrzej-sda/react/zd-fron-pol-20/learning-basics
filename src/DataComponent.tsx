import React from "react";
import { PersonalDetailsComponent, PersonalDetailsComponentProps } from './PersonalDetailsComponent';
import { TeleaddressDataComponent, TeleaddressDataComponentProps } from './TeleaddressDataComponent';

type DataComponentProps = PersonalDetailsComponentProps & TeleaddressDataComponentProps;

export function DataComponent(props: DataComponentProps): JSX.Element {
    return <div>
		<PersonalDetailsComponent personalDetails={props.personalDetails}/>
		{props.teleaddressData.street && <TeleaddressDataComponent
			teleaddressData={props.teleaddressData}
		/>}
    </div>;
}

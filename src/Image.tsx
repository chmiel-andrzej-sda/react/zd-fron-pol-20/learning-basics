import React from "react";

interface ImageProps {
    readonly src: string;
    readonly alt: string;
    readonly count: number;
    readonly onClick: (count: number) => void;
}

export function Image(props: ImageProps): JSX.Element {
    function handleClick(): void {
        props.onClick(props.count + 1);
    }

    return <div>
        <img
            src={props.src}
            alt={props.alt}
            width={480}
            height={640}
            onClick={handleClick}
        />
        <br/>
        {props.count}
    </div>;
}

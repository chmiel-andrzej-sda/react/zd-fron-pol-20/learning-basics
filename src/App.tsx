import React from 'react';
import './App.css';
import { Button } from './Button';
import { DataComponent } from './DataComponent';
import { Greeting } from './Greeting';
import { Image } from './Image';
import { PersonalDetails } from './entity/PersonalDetails';
import { PersonalDetailsComponent } from './PersonalDetailsComponent';

const array: PersonalDetails[] = [
	{firstName: 'Andrzej', lastName: 'Chmiel', pesel: 22222222222},
	{firstName: 'Andrzej', lastName: 'Chmiel', pesel: 22222222222},
	{firstName: 'Andrzej', lastName: 'Chmiel', pesel: 22222222222}
];

export function App(): JSX.Element {
	const [count, setCount] = React.useState<number>(0);

	function renderPersonalDetails(personalDetails: PersonalDetails, index: number): JSX.Element {
		return <PersonalDetailsComponent key={index} personalDetails={personalDetails}/>;
	}
	
	return <div className="App">
		<Greeting
			name='Andrzej'
		/>
		<Image
			alt='image'
			src='https://images.unsplash.com/photo-1595433707802-6b2626ef1c91?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80'
			onClick={setCount}
			count={count}
		/>
		<DataComponent
			personalDetails={{
				firstName: 'Andrzej',
				lastName: 'Chmiel',
				pesel: 22222222222
			}}
			teleaddressData={{
				street: 'Kolorowa 10/2a',
				city: 'Krakow',
				country: 'PL'
			}}
		/>
		<Button
			start={10}
			defaultLabel='CLICK ME'
			reverse
			count={count}
			onClick={setCount}
		/>
		{array.map(renderPersonalDetails)}
	</div>;
}

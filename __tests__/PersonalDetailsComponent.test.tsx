import React from "react";
import { ReactTestRenderer, create } from 'react-test-renderer';
import { PersonalDetailsComponent } from '../src/PersonalDetailsComponent';
import { personalDetails } from "./providers/TestDataProvider";

describe('PersonalDetailsComponent', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<PersonalDetailsComponent personalDetails={personalDetails}/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});
import React from "react";
import { ReactTestInstance, ReactTestRenderer, create } from "react-test-renderer";
import { Image } from '../src/Image';

describe('Image', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<Image
			src='test'
			alt='test'
			count={0}
			onClick={(): void => undefined}
		/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('clicks', (): void => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(<Image
			src='test'
			alt='test'
			count={0}
			onClick={mockClick}
		/>);

		// when
		const img: ReactTestInstance = component.root.findByType("img");
		img.props.onClick();

		// then
		expect(mockClick).toHaveBeenCalledTimes(1);
		expect(mockClick).toHaveBeenCalledWith(1);
	});
});

import React from 'react';
import { ReactTestInstance, ReactTestRenderer, create, act } from "react-test-renderer";
import { App } from '../src/App';

describe('App', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<App/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes count', (): void => {
		// given
		const component: ReactTestRenderer = create(<App/>);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		act(() => button.props.onClick());

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});

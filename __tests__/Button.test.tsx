import React from 'react';
import { ReactTestInstance, ReactTestRenderer, create } from 'react-test-renderer';
import { Button } from '../src/Button';

describe('Button', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<Button
			onClick={(): void => undefined}
			defaultLabel='abc'
			count={1}
		/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('renders with default label', (): void => {
		// when
		const component: ReactTestRenderer = create(<Button
			onClick={(): void => undefined}
			defaultLabel='abc'
			count={1}
			start={1}
		/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('clicks without reverse', (): void => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(<Button
			onClick={mockClick}
			defaultLabel='abc'
			count={1}
			start={1}
		/>);

		// when
		const button: ReactTestInstance = component.root.findByType("button");
		button.props.onClick();

		// then
		expect(mockClick).toHaveBeenCalledTimes(1);
		expect(mockClick).toHaveBeenCalledWith(2);
	});

	it('clicks with reverse', (): void => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const element: ReactTestRenderer = create(<Button
			onClick={mockClick}
			defaultLabel='abc'
			count={1}
			start={1}
			reverse
		/>);

		// when
		const button: ReactTestInstance = element.root.findByType("button");
		button.props.onClick();

		// then
		expect(mockClick).toHaveBeenCalledTimes(1);
		expect(mockClick).toHaveBeenCalledWith(0);
	});
});

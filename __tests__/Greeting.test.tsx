import React from "react";
import { ReactTestRenderer, create } from 'react-test-renderer';
import { Greeting } from '../src/Greeting';

describe('Greeting', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<Greeting name='Test'/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});

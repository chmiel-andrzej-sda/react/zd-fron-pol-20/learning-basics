import React from "react";
import { ReactTestRenderer, create } from "react-test-renderer";
import { TeleaddressDataComponent } from "../src/TeleaddressDataComponent";
import { teleaddressData } from "./providers/TestDataProvider";

describe("TeleaddressDataComponent", (): void => {
	it("renders", (): void => {
		// when
		const component: ReactTestRenderer = create(<TeleaddressDataComponent teleaddressData={teleaddressData}/>);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});
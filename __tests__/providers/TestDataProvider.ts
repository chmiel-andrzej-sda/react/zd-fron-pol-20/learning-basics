import { TeleadressData } from "../../src/entity/TeleaddressData";
import { PersonalDetails } from "../../src/entity/PersonalDetails";

export const personalDetails: PersonalDetails = {
    firstName: 'Test',
    lastName: 'Tester',
    pesel: 22222222222
};

export const teleaddressDataWithoutStreet: TeleadressData = {
    city: 'Kraków',
    country: 'PL'
};

export const teleaddressData: TeleadressData = {
    ...teleaddressDataWithoutStreet,
    street: 'Kolorowa'
};

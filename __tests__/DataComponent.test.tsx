import React from "react";
import { ReactTestRenderer, create } from "react-test-renderer";
import { DataComponent } from '../src/DataComponent';
import { personalDetails, teleaddressData, teleaddressDataWithoutStreet } from "./providers/TestDataProvider";

describe('DataComponent', (): void => {
	it('renders with street', (): void => {
		// when
		const component: ReactTestRenderer = create(<DataComponent 
			personalDetails={personalDetails}
			teleaddressData={teleaddressData}
		/>);
  
		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('renders without street', (): void => {
		// when
		const component: ReactTestRenderer = create(<DataComponent 
			personalDetails={personalDetails}
			teleaddressData={teleaddressDataWithoutStreet}
		/>);
  
		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});
